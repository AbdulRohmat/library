package http

import (
	"library/constant"
	"library/domain/model"
	"library/usecase"

	"github.com/gofiber/fiber/v2"
)

type libaryHandler struct {
	libaryUsecase usecase.ILibaryUsecase
}

func NewLibaryHandler(libaryUsecase usecase.ILibaryUsecase) *libaryHandler {
	return &libaryHandler{
		libaryUsecase: libaryUsecase,
	}
}

func (h *libaryHandler) ListBookHandler(c *fiber.Ctx) error {
	req := model.ListBooksReq{}
	c.BodyParser(&req)

	res := model.BaseResponse{
		ResponseCode: constant.RC_SUCCESS,
		ResponseDesc: constant.RD_SUCCESS,
	}

	resData, err := h.libaryUsecase.ListBookUsecase(&req)
	if err != nil {
		res.ResponseCode = constant.RC_FAILED
		res.ResponseDesc = err.Error()

		return model.Response(c, res)
	}

	res.Data = resData
	return model.Response(c, res)
}

func (h *libaryHandler) LoanBookHandler(c *fiber.Ctx) error {
	// req := model.ListBooksReq{}
	// c.BodyParser(&req)

	bookCode := c.Params("bookCode")

	res := model.BaseResponse{
		ResponseCode: constant.RC_SUCCESS,
		ResponseDesc: constant.RD_SUCCESS,
	}

	err := h.libaryUsecase.LoanBookUsecase(bookCode)
	if err != nil {
		res.ResponseCode = constant.RC_FAILED
		res.ResponseDesc = err.Error()

		return model.Response(c, res)
	}

	return model.Response(c, res)
}

func (h *libaryHandler) ReturnBookHandler(c *fiber.Ctx) error {
	// req := model.ListBooksReq{}
	// c.BodyParser(&req)

	historyId := c.Params("historyId")

	res := model.BaseResponse{
		ResponseCode: constant.RC_SUCCESS,
		ResponseDesc: constant.RD_SUCCESS,
	}

	err := h.libaryUsecase.ReturnBookUsecase(historyId)
	if err != nil {
		res.ResponseCode = constant.RC_FAILED
		res.ResponseDesc = err.Error()

		return model.Response(c, res)
	}

	return model.Response(c, res)
}
