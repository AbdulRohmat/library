package usecase

import (
	"errors"
	"library/domain/model"
	"library/domain/repository"
	"time"

	"gorm.io/gorm"
)

type libaryUsecase struct {
	conn     *gorm.DB
	mBook    repository.IMBookRepo
	tHistory repository.ITHisotryRepo
}

type ILibaryUsecase interface {
	ListBookUsecase(req *model.ListBooksReq) (*[]model.ListBooksResp, error)
	LoanBookUsecase(bookCode string) error
	ReturnBookUsecase(historyId string) error
}

func NewLibaryUsecase(
	conn *gorm.DB,
	mBook repository.IMBookRepo,
	tHistory repository.ITHisotryRepo,

) ILibaryUsecase {
	return &libaryUsecase{
		conn:     conn,
		mBook:    mBook,
		tHistory: tHistory,
	}
}

func (u *libaryUsecase) ListBookUsecase(req *model.ListBooksReq) (*[]model.ListBooksResp, error) {

	data, err := u.mBook.GetListBooks(*req)
	if err != nil || len(data) == 0 {
		return nil, err
	}

	for _, val := range data {

		value := model.ListBooksResp{
			BookName:      val.BookName,
			BookCode:      val.BookCode,
			Genre:         val.Genre,
			GenreCode:     val.GenreCode,
			Author:        val.Author,
			EditionNumber: val.EditionNumber,
			Qty:           val.Qty,
		}

		h := model.THistory{
			BookID: val.BookID,
		}
		history, _ := u.tHistory.FindData(h)

		value.History = history

		data = append(data, value)
	}

	return &data, nil
}

func (u *libaryUsecase) LoanBookUsecase(bookCode string) error {

	var err error
	var currentTime time.Time

	// get data book
	mBook := model.MBooks{
		Code: bookCode,
	}

	book, err := u.mBook.FindOne(mBook)
	// validasi book qty
	if err != nil || book.Qty == 0 {
		errors.New("Books Empty")
	}

	// db transaction for validation
	if err := u.conn.Transaction(func(ctx *gorm.DB) error {

		// Save Data History
		tHistory := model.THistory{
			BookID:    book.ID,
			StartDate: currentTime,
			ExpDate:   currentTime.AddDate(0, 0, book.Exp),
		}

		err = u.tHistory.SaveData(tHistory)
		if err != nil {
			return err
		}

		// update qty book
		mBook.ID = book.ID
		mBook.Qty = book.Qty - 1
		err = u.mBook.UpdateData(mBook)
		if err != nil {
			return err
		}

		return nil
	}); err != nil {
		return err
	}

	return nil
}

func (u *libaryUsecase) ReturnBookUsecase(historyId string) error {

	var err error
	var currentTime time.Time

	// db transaction for validation
	if err = u.conn.Transaction(func(ctx *gorm.DB) error {

		// Get data history
		tHistory := model.THistory{
			ID: historyId,
		}

		historyData, err := u.tHistory.FindOne(tHistory)
		if err != nil {
			return err
		}

		tHistory.BookID = historyData.BookID
		tHistory.EndDate = currentTime
		tHistory.ExpRef = historyData.ID

		err = u.tHistory.SaveData(tHistory)

		// get data book
		mBook := model.MBooks{
			ID: historyData.BookID,
		}

		book, err := u.mBook.FindOne(mBook)
		// validasi book qty
		if err != nil {
			errors.New("Books Empty")
		}

		// update qty book
		mBook.ID = book.ID
		mBook.Qty = book.Qty + 1
		err = u.mBook.UpdateData(mBook)
		if err != nil {
			return err
		}

		return nil
	}); err != nil {
		return err
	}

	return nil
}
