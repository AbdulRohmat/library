package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"library/infrastructure/persistence"
	http "library/interfaces/http"
	"library/usecase"
	httpOri "net/http"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/gofiber/helmet/v2"
	"golang.org/x/sync/errgroup"
)

var (
	port = "3300"

	httpServer *httpOri.Server
)

func main() {
	// // start init config using yml
	// err := configor.New(&configor.Config{ErrorOnUnmatchedKeys: false}).Load(&shared.Config, "config.yml")
	// if err != nil {
	// 	log.Fatalf("ERROR: %s", err.Error())
	// }
	// // end init config using yml

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)
	defer signal.Stop(interrupt)

	g, ctx := errgroup.WithContext(ctx)

	db, err := persistence.NewRepositories()
	if err != nil {
		log.Fatalf("ERROR: %s", err.Error())
	}

	// init repository
	mBookRepo := persistence.NewMBooksRepo(db)
	tHistoryRepo := persistence.NewTHistory(db)
	// end init repositories

	// start init usecase
	libaryUc := usecase.NewLibaryUsecase(db, mBookRepo, tHistoryRepo)
	// end init usecase

	// start init handler
	libaryHandler := http.NewLibaryHandler(libaryUc)
	// end init handler

	// init services with http (gofiber)
	// init server with config
	cfgFiber := fiber.Config{
		BodyLimit: 5 * 1024 * 1024, // 100 mb limit upload
	}
	app := fiber.New(cfgFiber)
	// recover
	app.Use(recover.New())
	// cors default
	app.Use(cors.New(cors.Config{
		AllowOrigins:     "*",
		AllowMethods:     "GET,POST,HEAD,PUT,DELETE",
		AllowHeaders:     "Origin, authorization, Content-Length, Content-Type, User-Agent, Referrer, Host, Token",
		ExposeHeaders:    "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type",
		AllowCredentials: true,
		MaxAge:           86400,
	}))
	// helmet default
	app.Use(helmet.New())

	// init rest handler
	// TRANSACTION SERVICE
	libary := app.Group("libary")
	libary.Post("list", libaryHandler.ListBookHandler)
	libary.Post("loan:/bookCode", libaryHandler.LoanBookHandler)
	libary.Post("return/:historyId", libaryHandler.ReturnBookHandler)

	g.Go(func() error {

		httpServer = &httpOri.Server{
			// Handler:      app,
			Addr:         fmt.Sprintf(":%s", port),
			ReadTimeout:  10 * time.Second,
			WriteTimeout: 10 * time.Second,
		}

		if err := httpServer.ListenAndServe(); err != httpOri.ErrServerClosed {
			return err
		}

		return nil
	})

	select {
	case <-interrupt:
		break
	case <-ctx.Done():
		break
	}

	cancel()
	shutdownCtx, shutdownCancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer shutdownCancel()

	if httpServer != nil {
		_ = httpServer.Shutdown(shutdownCtx)
	}

	err = g.Wait()
	if err != nil {
		log.Print("server returning an error ", err)
		os.Exit(2)
	}
}
