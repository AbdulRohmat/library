package model

import (
	"time"

	"github.com/google/uuid"
)

type MGenre struct {
	ID        string
	Name      string
	Code      string
	CreatedAt time.Time
	CreatedBy string
	UpdatedAt time.Time
	UpdatedBy string
}

func (m *MGenre) TableName() string {
	return "public.m_genre"
}

func NewMGenre(
	Name string,
	Code string,
) *MGenre {
	now := time.Now()
	return &MGenre{
		ID:        uuid.NewString(),
		Name:      Name,
		Code:      Code,
		CreatedAt: now,
		CreatedBy: "System",
		UpdatedAt: now,
		UpdatedBy: "System",
	}
}
