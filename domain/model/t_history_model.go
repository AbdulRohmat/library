package model

import (
	"time"

	"github.com/google/uuid"
)

type THistory struct {
	ID        string
	BookID    string
	StartDate time.Time
	EndDate   time.Time
	ExpDate   time.Time
	ExpRef    string
	CreatedAt time.Time
	CreatedBy string
	UpdatedAt time.Time
	UpdatedBy string
}

func (t *THistory) TableName() string {
	return "public.t_history"
}

func NewTHistory(
	BookID string,
	StartDate time.Time,
	EndDate time.Time,
	ExpDate time.Time,
	ExpRef string,

) *THistory {
	now := time.Now()
	return &THistory{
		ID:        uuid.NewString(),
		BookID:    BookID,
		StartDate: StartDate,
		EndDate:   EndDate,
		ExpDate:   ExpDate,
		ExpRef:    ExpRef,
		CreatedAt: now,
		CreatedBy: "System",
		UpdatedAt: now,
		UpdatedBy: "System",
	}
}
