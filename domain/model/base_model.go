package model

import (
	"net/http"

	"github.com/gofiber/fiber/v2"
)

type BaseResponse struct {
	ResponseCode string      `json:"responseCode"`
	ResponseDesc string      `json:"responseDesc"`
	Data         interface{} `json:"data"`
}

// Response base response from fiber
func Response(c *fiber.Ctx, res interface{}) error {
	return c.Status(http.StatusOK).JSON(res)
}

func ResponseError(c *fiber.Ctx, statusCode int, res interface{}) error {
	return c.Status(statusCode).JSON(res)
}
