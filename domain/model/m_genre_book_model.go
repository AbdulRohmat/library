package model

import (
	"time"

	"github.com/google/uuid"
)

type MGenreBook struct {
	ID        string
	BookID    string
	GenreID   string
	CreatedAt time.Time
	CreatedBy string
	UpdatedAt time.Time
	UpdatedBy string
}

func (m *MGenreBook) TableName() string {
	return "public.m_books"
}

func NewMGenreBook(
	BookID string,
	GenreID string,
) *MGenreBook {
	now := time.Now()
	return &MGenreBook{
		ID:        uuid.NewString(),
		BookID:    BookID,
		GenreID:   GenreID,
		CreatedAt: now,
		CreatedBy: "System",
		UpdatedAt: now,
		UpdatedBy: "System",
	}
}
