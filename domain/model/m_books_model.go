package model

import (
	"time"

	"github.com/google/uuid"
)

type MBooks struct {
	ID            string
	Name          string
	Code          string
	Qty           int
	Author        string
	EditionNumber int
	Exp           int
	CreatedAt     time.Time
	CreatedBy     string
	UpdatedAt     time.Time
	UpdatedBy     string
}

func (m *MBooks) TableName() string {
	return "public.m_books"
}

func NewMBooks(
	Name string,
	Code string,
	Qty int,
	Author string,
	EditionNumber int,
	Exp int,
) *MBooks {
	now := time.Now()
	return &MBooks{
		ID:            uuid.NewString(),
		Name:          Name,
		Code:          Code,
		Qty:           Qty,
		Author:        Author,
		EditionNumber: EditionNumber,
		Exp:           Exp,
		CreatedAt:     now,
		CreatedBy:     "System",
		UpdatedAt:     now,
		UpdatedBy:     "System",
	}
}

// >>>>> List Book <<<<<

type ListBooksReq struct {
	Book      string
	BookCode  string
	Genre     string
	GenreCode string
}

type ListBooksResp struct {
	BookID        string `json:"-" gorm:"book_id"`
	BookName      string
	BookCode      string
	Genre         string
	GenreCode     string
	Author        string
	EditionNumber int
	Qty           int
	History       []THistory
}
