package repository

import "library/domain/model"

type IMBookRepo interface {
	GetListBooks(reqData model.ListBooksReq) ([]model.ListBooksResp, error)
	FindOne(reqData model.MBooks) (model.MBooks, error)
	UpdateData(reqData model.MBooks) error
}
