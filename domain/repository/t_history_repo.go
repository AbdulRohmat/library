package repository

import "library/domain/model"

type ITHisotryRepo interface {
	FindData(reqData model.THistory) ([]model.THistory, error)
	FindOne(reqData model.THistory) (model.THistory, error)
	SaveData(reqData model.THistory) error
	UpdateData(reqData model.THistory) error
}
