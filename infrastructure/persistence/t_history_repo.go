package persistence

import (
	"library/domain/model"
	"library/domain/repository"

	"gorm.io/gorm"
)

type tHistory struct {
	db *gorm.DB
}

func NewTHistory(db *gorm.DB) repository.ITHisotryRepo {
	return &tHistory{db: db}
}

func (t *tHistory) FindData(reqData model.THistory) ([]model.THistory, error) {

	resp := []model.THistory{}
	err := t.db.Where(reqData).Find(&resp).Error
	if err != nil {
		return resp, err
	}

	return resp, nil
}

func (t *tHistory) FindOne(reqData model.THistory) (model.THistory, error) {

	resp := model.THistory{}
	err := t.db.Where(reqData).First(&resp).Error
	if err != nil {
		return resp, err
	}

	return resp, nil
}

func (t *tHistory) SaveData(reqData model.THistory) error {

	err := t.db.Create(&reqData).Error
	if err != nil {
		return err
	}

	return nil
}

func (t *tHistory) UpdateData(reqData model.THistory) error {
	return t.db.Model(&model.THistory{}).Where("id = ?", reqData.ID).Updates(&reqData).Error
}
