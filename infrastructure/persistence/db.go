package persistence

import (
	"fmt"
	"log"

	"github.com/kelseyhightower/envconfig"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Config struct {
	Host     string `envconfig:"POSTGRES_HOST" default:"localhost"`
	Port     string `envconfig:"POSTGRES_PORT" default:"5432"`
	User     string `envconfig:"POSTGRES_USER" default:"postgres"`
	Password string `envconfig:"POSTGRES_PASSWORD" default:"?????"`
	Database string `envconfig:"POSTGRES_DATABASE" default:"?????"`
}

var (
	cfg *Config = &Config{}
)

func init() {
	err := envconfig.Process("LOCAL", cfg)
	if err != nil {
		log.Fatalf("Failed to get LOCAL env %v", err)
	}
}

func NewRepositories() (*gorm.DB, error) {
	// init connection postgres
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable",
		cfg.Host, cfg.User, cfg.Password, cfg.Database, cfg.Port)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatalf("ERROR: %s", err.Error())
	}
	log.Printf("INFO: Connected to DB")

	return db, nil
}
