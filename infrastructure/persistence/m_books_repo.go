package persistence

import (
	"library/domain/model"
	"library/domain/repository"

	"gorm.io/gorm"
)

type mBooksRepo struct {
	db *gorm.DB
}

func NewMBooksRepo(db *gorm.DB) repository.IMBookRepo {
	return &mBooksRepo{db: db}
}

func (m *mBooksRepo) GetListBooks(reqData model.ListBooksReq) ([]model.ListBooksResp, error) {

	var resp []model.ListBooksResp

	query := m.db.Table("m_books as a").Select("a.id as book_id, a.name as book_name, a.code as book_code, c.name as genre_name, c.code as genre_code, a.author, a.edition_number, a.qty").
		Joins("join genre_book as b on a.id = b.id_book").
		Joins("join genre as c on c.id = b.id_genre")

	if reqData.BookCode != "" {
		query.Where("LOWER(a.code) like LOWER('%" + reqData.BookCode + "%')")
	}

	if reqData.Book != "" {
		query.Where("LOWER(a.name) like LOWER('%" + reqData.Book + "%')")
	}

	if reqData.Genre != "" {
		query.Where("LOWER(c.name) like LOWER('%" + reqData.Genre + "%')")
	}

	if reqData.GenreCode != "" {
		query.Where("LOWER(c.code) like LOWER('%" + reqData.GenreCode + "%')")
	}

	err := query.Scan(&resp).Error
	if err != nil {
		return resp, err
	}

	return resp, nil
}

func (m *mBooksRepo) FindOne(reqData model.MBooks) (model.MBooks, error) {
	resp := model.MBooks{}

	err := m.db.Where(reqData).Find(&resp).Error
	if err != nil {
		return resp, err
	}

	return resp, nil
}

func (m *mBooksRepo) UpdateData(reqData model.MBooks) error {
	return m.db.Model(&model.MBooks{}).Where("id = ?", reqData.ID).Update("qty", reqData.Qty).Error
}
